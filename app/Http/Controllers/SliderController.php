<?php

namespace App\Http\Controllers;
use App\Models\slider;
use Image;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SliderController extends Controller
{
    public function homeslider()
    {
        $slider=slider::latest()->get();
        return view('admin.slider.index',compact('slider'));

    }

    public function addslider()
    {
        return view('admin.slider.create');
    }

    public function storelider(Request $request)
    {
        $validated = $request->validate([
            'title' => 'required|min:5|max:100',
            'description' => 'required|min:10',
            'image' => 'required|mimes:jpg,jpeg,png',
            ],
            [
                'title.required' => 'please fill title',
                'title.min' => ' longer than 5 char',
                'description.required' => 'please fill the description',
                'description.min' => ' must be longer than 10 char'
            ]);

            $slider=$request->file('image');

                $name_gen= hexdec(uniqid()).'.'.$slider->getClientOriginalExtension();
                Image::make($slider)->resize('1920','1088')->save('image/slider/'.$name_gen);
                $slide='image/slider/'.$name_gen;


            slider::insert([
                'title'=>$request->title,
                'description'=>$request->description,
                'image'=>$slide,
                'created_at'=>Carbon::now()

            ]);
            return redirect()->route('home.slider')->with('success','slider added successfully');
    }


    public function editslider($id)
    {
        $slider=slider::findorfail($id);
        return view('admin.slider.edit',compact('slider'));
    }

    public function updateslider(Request $request, $id)
    {
        $validated = $request->validate([
            'title' => 'required|min:5|max:100',
            'description' => 'required|min:10',
            'image' => 'mimes:jpg,jpeg,png',
            ],
            [
                'title.required' => 'please fill title',
                'title.min' => ' longer than 5 char',
                'description.required' => 'please fill the description',
                'description.min' => ' must be longer than 10 char'
            ]);


                $image=$request->file('image');
                if ($image) {
                    $name_gen= hexdec(uniqid()).'.'.$image->getClientOriginalExtension();
                    Image::make($image)->resize('1920','1088')->save('image/slider/'.$name_gen);
                    $slide='image/slider/'.$name_gen;

                        $old=$request->old_image;
                        unlink($old);
                        slider::findorfail($id)->update([
                    'title'=>$request->title,
                    'description'=>$request->description,
                    'image'=>$slide,
                    'updated_at'=>Carbon::now()

                        ]);
                        return redirect()->back()->with('success', 'brand updated successfuly');
                }else{

                    slider::findorfail($id)->update([
                        'title'=>$request->title,
                        'description'=>$request->description,
                        'updated_at'=>Carbon::now()

                            ]);
                            return redirect()->back()->with('success', 'brand updated successfuly');

                }
        }

            public function delete($id)
            {
                $slider=slider::findorfail($id);
                $old_img=$slider->image;
                unlink($old_img);

                slider::findorfail($id)->delete();
                return redirect()->back()->with('success', 'brand deleted successfuly');
            }
}
