<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Models\multipic;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Image;
// use Intervention\Image\Image;

class BrandController extends Controller
{
    public function allbrand()
    {
        $brands=brand::latest()->paginate(5);
        return view('admin.brand.index', compact('brands'));
    }

    public function addbrand(Request $request)
    {
        $validated = $request->validate([
            'brand_name' => 'required|unique:brands|min:4',
            'brand_image' => 'required|mimes:jpg,jpeg,png',
            ],
            [
                'brand_name.required' => 'please fill brand name',
                'brand_name.min' => 'brand longer than 4 char'

            ]);
                $brand_image=$request->file('brand_image');

                // $name_gen=hexdec(uniqid());

                // $img_ext=strtolower($brand_image->getClientOriginalExtension());
                // $img_name=$name_gen .'.'. $img_ext;
                // $up_location='image/brand/';
                // $last_img=$up_location.$img_name;
                // $brand_image->move($up_location,$img_name);

                $name_gen= hexdec(uniqid()).'.'.$brand_image->getClientOriginalExtension();
                Image::make($brand_image)->resize('200','250')->save('image/brand/'.$name_gen);
                $last_img='image/brand/'.$name_gen;


            brand::insert([
                'brand_name'=>$request->brand_name,
                'brand_image'=>$last_img,
                'created_at'=>Carbon::now()

            ]);
            return redirect()->back()->with('success','brand added successfuly');
    }




    public function editbrand($id)
    {
        $brand=Brand::findorfail($id);
        return view('admin.brand.edit',compact('brand'));
    }

    public function updatebrand(Request $request, $id)
    {
        $validated = $request->validate([
            'brand_name' => 'required|min:4',

            ],
            [
                'brand_name.required' => 'please fill brand name',
                'brand_name.min' => 'brand longer than 4 char'

            ]);

                $old=$request->old_image;
                $brand_image=$request->file('brand_image');
                if ($brand_image) {
                        $name_gen=hexdec(uniqid());
                        $img_ext=strtolower($brand_image->getClientOriginalExtension());
                        $img_name=$name_gen.'.'.$img_ext;
                        $up_location='image/brand/';
                        $last_img=$up_location.$img_name;
                        $brand_image->move($up_location, $img_name);

                        unlink($old);
                        brand::findorfail($id)->update([
                    'brand_name'=>$request->brand_name,
                    'brand_image'=>$last_img,
                    'created_at'=>Carbon::now()

                        ]);
                        return redirect()->back()->with('success', 'brand updated successfuly');
                }else{

                    brand::findorfail($id)->update([
                        'brand_name'=>$request->brand_name,
                        'created_at'=>Carbon::now()

                            ]);
                            return redirect()->back()->with('success', 'brand updated successfuly');

                }
        }

            public function delete($id)
            {
                $image=Brand::findorfail($id);
                $old_img=$image->brand_image;
                unlink($old_img);

                Brand::findorfail($id)->delete();
                return redirect()->back()->with('success', 'brand deleted successfuly');
            }

// function for images

public function multipic()
{
    $images=multipic::all();

    return view('admin.images.index',compact('images'));
}





public function storeimg(Request $request)
{
            $image=$request->file('image');

            foreach ($image as $multi_img) {

            $name_gen= hexdec(uniqid()).'.'.$multi_img->getClientOriginalExtension();
            Image::make($multi_img)->resize('200','250')->save('image/multi/'.$name_gen);
            $last_img='image/multi/'.$name_gen;


        multipic::insert([
            'image'=>$last_img,
            'created_at'=>Carbon::now()
            ]);
            }//end foreach
            return redirect()->back()->with('success','images added successfuly');

}





































}
