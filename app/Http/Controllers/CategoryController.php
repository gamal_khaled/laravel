<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;



use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Category;
use Carbon\Carbon;

class CategoryController extends Controller
{
    public function allcat()
    {
        $categories=Category::latest()->paginate(5);
        $trashcat=Category::onlyTrashed()->latest()->paginate(3);
        return view('admin.category.index',compact('categories','trashcat'));
    }




    public function addcat( Request $request)
    {
        $validated = $request->validate([
            'category_name' => 'required|unique:categories|max:255',
            ],
            [
                'category_name.required' => 'please fill category name',

            ]);
            Category::insert([
                'category_name'=>$request->category_name,
                'user_id'=>Auth::user()->id,
                'created_at'=>Carbon::now()

            ]);
            return redirect()->back()->with('success','the category added successfully');

    }


    public function editcat($id)
    {
        $categories=Category::findorfail($id);
        return view('admin.category.edit',compact('categories'));
    }

    public function updatecat(Request $request,$id)
    {
        Category::findorfail($id)->update([
            'category_name'=>$request->category_name,
            'user_id'=>Auth::user()->id,
            'updated_at'=>Carbon::now()

        ]);
         return redirect()->route('all.category')->with('success','the category updated successfully');
    }


    public function softdelete($id)
    {
       $delete=Category::find($id)->delete();
       return redirect()->back()->with('success','the category deleted successfully');
    }


    public function restorecat($id)
    {
        $restore=Category::withTrashed()->find($id)->restore();
        return redirect()->back()->with('success','the category restored successfully');
    }

    public function pdeletcat($id )
    {
        $pdelete=Category::onlyTrashed()->find($id)->forcedelete();
        return redirect()->back()->with('success','the category permanently deleted successfully');
    }
}
