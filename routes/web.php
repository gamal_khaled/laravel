<?php

use App\Http\Controllers\AboutController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\BrandController;
use App\Http\Controllers\SliderController;
use Illuminate\Support\Facades\Route;
use App\Models\User;
use App\Http\Controllers\CategoryController;
use App\Models\Brand;
use App\Models\HomeAbout;
use App\Models\multipic;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\ChangePass;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
     $brands=brand::all();
     $abouts=HomeAbout::first();
     $images=multipic::all();
    return view('home',compact('brands','abouts','images'));
});






Route::get('/create', function () {
    return view('form');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {

    $users=user::all();
    return view('admin.index');
})->name('dashboard');

Route::get('category/all',[CategoryController::class,'allcat'])->name('all.category');

Route::post('category/all',[CategoryController::class,'addcat'])->name('store.category');

Route::get('category/edit/{id}', [CategoryController::class,'editcat'])->name('edit.category');
Route::post('category/update/{id}', [CategoryController::class,'updatecat'])->name('update.category');
Route::get('category/softdelete/{id}', [CategoryController::class,'softdelete'])->name('softdelete.category');
Route::get('category/restore/{id}', [CategoryController::class,'restorecat'])->name('restore.category');
Route::get('category/delete/{id}', [CategoryController::class,'pdeletcat'])->name('pdelete.category');



///for brand:

Route::get('brand/all',[BrandController::class,'allbrand'])->name('all.brand');
Route::post('brand/all',[BrandController::class,'addbrand'])->name('store.brand');
Route::get('brand/edit/{id}', [brandController::class,'editbrand'])->name('edit.brand');
Route::post('brand/update/{id}', [brandController::class,'updatebrand'])->name('update.brand');
Route::get('brand/delete/{id}', [brandController::class,'delete'])->name('delete.brand');

//image routes:
Route::get('image/all',[BrandController::class,'multipic'])->name('all.image');
Route::post('multi/add',[BrandController::class,'storeimg'])->name('store.image');




//slider

Route::get('home/slider', [SliderController::class,'homeslider'])->name('home.slider');
Route::get('add/slider', [SliderController::class,'addslider'])->name('add.slider');
Route::post('store/slider', [SliderController::class,'storelider'])->name('store.slider');
Route::get('slider/edit/{id}', [SliderController::class,'editslider'])->name('edit.slider');
Route::post('slider/update/{id}', [SliderController::class,'updateslider'])->name('update.slider');
Route::get('slider/delete/{id}', [SliderController::class,'delete'])->name('delete.slider');


//about page
Route::get('/home/About', [AboutController::class, 'HomeAbout'])->name('home.about');
Route::get('/add/About', [AboutController::class, 'AddAbout'])->name('add.about');
Route::post('/store/About', [AboutController::class, 'StoreAbout'])->name('store.about');
Route::get('/about/edit/{id}', [AboutController::class, 'EditAbout'])->name('edit.about');
Route::post('/update/homeabout/{id}', [AboutController::class, 'UpdateAbout'])->name('update.about');
Route::get('/about/delete/{id}', [AboutController::class, 'DeleteAbout'])->name('delete.about');


//Portfolio Page Route
Route::get('/portfolio', [AboutController::class, 'Portfolio'])->name('portfolio');

// Amdin Contact Page Route
Route::get('/admin/contact', [ContactController::class, 'AdminContact'])->name('admin.contact');
Route::get('/admin/add/contact', [ContactController::class, 'AdminAddContact'])->name('add.contact');
Route::post('/admin/store/contact', [ContactController::class, 'AdminStoreContact'])->name('store.contact');
Route::get('/admin/message', [ContactController::class, 'AdminMessage'])->name('admin.message');




/// Home Contact Page Route
Route::get('/contact', [ContactController::class, 'Contact'])->name('contact');
Route::post('/contact/form', [ContactController::class, 'ContactForm'])->name('contact.form');


/// Chanage Password and user Profile Route
Route::get('/user/password', [ChangePass::class, 'CPassword'])->name('change.password');
Route::post('/password/update', [ChangePass::class, 'UpdatePassword'])->name('password.update');


//login and logout

Route::get('user/logout',[AuthController::class,'logout'])->name('user.logout');


// User Profile
Route::get('/user/profile', [ChangePass::class, 'PUpdate'])->name('profile.update');
Route::post('/user/profile/update', [ChangePass::class, 'UpdateProfile'])->name('update.user.profile');







