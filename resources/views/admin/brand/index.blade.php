
@extends('admin.admin_master')


@section('admin')


    <div class="container">
        <div class="mt-5">
            <div class="row">
                <div class="col-md-8">
                        @if (session('success'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <strong>{{session('success')}}</strong>
                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                            </div>
                            @endif
                    <div class="card">
                        <div class="card-header">All Brand</div>
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">ID</th>
                                    <th scope="col">brand</th>
                                    <th scope="col">image</th>
                                    <th scope="col">Created At</th>
                                    <th scope="col">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                    {{-- @php
                                        $i=1;
                                    @endphp --}}
                                    @foreach ($brands as $brand)


                                <tr>
                                    <th scope="row">{{$brands->firstitem()+$loop->index}}</th>
                                    <td>{{$brand->brand_name}}</td>
                                    <td><img src="{{asset($brand->brand_image)}}"style="width:50px ; height:50px " alt=""></td>
                                    <td>@if($brand->created_at ==NULL)
                                        <SPAN class="text-danger">date not found</SPAN>
                                        @else
                                        {{$brand->created_at->diffforhumans()}}
                                        @endif
                                    </td>
                                    <td>
                                        <a class="btn btn-primary" href="{{route('edit.brand',['id'=>$brand->id])}}" >edit</a>
                                        <a class="btn btn-danger" onclick="return confirm('Are you sure to delete')" href="{{route('delete.brand',['id'=>$brand->id])}}" >delete</a>
                                    </td>
                                        @endforeach
                                </tr>

                                </tbody>

                            </table>{{$brands->links()}}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header">add brand</div>
                            <div class="card-body">
                                <form action="{{route('store.brand')}}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <div class="mb-3">
                                        <label for="cat" class="form-label">brand</label>
                                        <input type="text" class="form-control" id="cat" name="brand_name" aria-describedby="emailHelp">
                                        @error('brand_name')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror

                                        </div>
                                    <div class="mb-3">
                                    <label for="cat" class="form-label">brand image</label>
                                    <input type="file" class="form-control" id="cat" name="brand_image" aria-describedby="emailHelp">

                                    @error('brand_image')
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                    </div>
                                    <button type="submit" class="btn btn-primary">Add brand</button>
                                </form>
                            </div>
                    </div>
                </div>
            </div>
            </div>
    </div>


    @endsection






