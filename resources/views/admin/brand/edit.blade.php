@extends('admin.admin_master')
@section('admin')


    <div class="container">
        <div class="mt-5">
            <div class="row">
                <div class="col-md-8">
                    @if (session('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>{{session('success')}}</strong>
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                    @endif
                    <div class="card">
                        <div class="card-header">Edit Brand</div>
                        <div class="card-body">
                            <form action="{{route('update.brand',['id'=>$brand->id])}}" enctype="multipart/form-data" method="POST">
                                @csrf
                                <input type="hidden" name="old_image" value="{{$brand->brand_image}}">
                                    <div class="mb-3">
                                    <label for="cat" class="form-label">Edit Brand Name</label>
                                    <input type="text" class="form-control" id="cat" value="{{$brand->brand_name}}"
                                            name="brand_name" aria-describedby="emailHelp">

                                    @error('category_name')
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                    </div>
                                    <div class="mb-3">
                                        <label for="cat" class="form-label">Edit Brand image</label>
                                        <input type="file" class="form-control" id="cat" name="brand_image"
                                                aria-describedby="emailHelp">

                                        @error('brand_image')
                                            <span class="text-danger">{{$message}}</span>
                                        @enderror
                                        </div>
                                        <div class="form-group">
                                            <img src="{{asset($brand->brand_image)}}" style="width:200px; height=;250px">
                                        </div>
                                    <button type="submit" class="btn btn-primary mt-2">Update brand</button>
                                </form>
                            </div>
                    </div>
                </div>
            </div>
            </div>
    </div>



    @endsection









