@extends('admin.admin_master')
@section('admin')


    <div class="container">
        <div class="mt-5">
            <div class="row">
                <div class="col-md-8">
                        @if (session('success'))



                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <strong>{{session('success')}}</strong>
                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                            </div>
                            @endif
                    <div class="card">
                        <div class="card-header">All category</div>
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">ID</th>
                                    <th scope="col">category</th>
                                    <th scope="col">User</th>
                                    <th scope="col">Created At</th>
                                    <th scope="col">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                    {{-- @php
                                        $i=1;
                                    @endphp --}}
                                    @foreach ($categories as $cat)


                                <tr>
                                    <th scope="row">{{$categories->firstitem()+$loop->index}}</th>
                                    <td>{{$cat->category_name}}</td>
                                    <td>{{$cat->user->name}}</td>
                                    <td>@if($cat->created_at ==NULL)
                                        <SPAN class="text-danger">date not found</SPAN>
                                        @else
                                        {{$cat->created_at->diffforhumans()}}
                                        @endif
                                    </td>
                                    <td>
                                        <a class="btn btn-primary" href="{{route('edit.category',['id'=>$cat->id])}}" >edit</a>
                                        <a class="btn btn-danger" href="{{route('softdelete.category',['id'=>$cat->id])}}" >move to trash</a>
                                    </td>
                                        @endforeach
                                </tr>

                                </tbody>

                            </table>{{$categories->links()}}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header">All category</div>
                            <div class="card-body">
                                <form action="{{route('store.category')}}" method="POST">
                                    @csrf
                                    <div class="mb-3">
                                    <label for="cat" class="form-label">Category</label>
                                    <input type="text" class="form-control" id="cat" name="category_name" aria-describedby="emailHelp">

                                    @error('category_name')
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                    </div>
                                    <button type="submit" class="btn btn-primary">Add category</button>
                                </form>
                            </div>
                    </div>
                </div>
            </div>
            </div>
    </div>







    {{-- trashed part --}}


    <div class="container">
        <div class="mt-5">
            <div class="row">
                <div class="col-md-8">

                    <div class="card">
                        <div class="card-header">trash list</div>
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">ID</th>
                                    <th scope="col">category</th>
                                    <th scope="col">User</th>
                                    <th scope="col">deleted At</th>
                                    <th scope="col">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach ($trashcat as $cat)
                                <tr>
                                    <th scope="row">{{$categories->firstitem()+$loop->index}}</th>
                                    <td>{{$cat->category_name}}</td>
                                    <td>{{$cat->user->name}}</td>
                                    <td>@if($cat->deleted_at ==NULL)
                                        <SPAN class="text-danger">date not found</SPAN>
                                        @else
                                        {{$cat->deleted_at->diffforhumans()}}
                                        @endif
                                    </td>
                                    <td>
                                        <a class="btn btn-warning" href="{{route('restore.category',['id'=>$cat->id])}}" >resote</a>
                                        <a class="btn btn-danger" href="{{route('pdelete.category',['id'=>$cat->id])}}" >delete</a>
                                    </td>
                                        @endforeach
                                </tr>
                                </tbody>
                            </table>{{$trashcat->links()}}
                    </div>
                </div>
                <div class="col-md-4">
                </div>
            </div>
            </div>
    </div>

    @endsection
