@extends('admin.admin_master')
@section('admin')


    <div class="container">
        <div class="mt-5">
            <div class="row">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">Edit category</div>
                            <div class="card-body">
                                <form action="{{route('update.category',['id'=>$categories->id])}}" method="POST">
                                    @csrf
                                    <div class="mb-3">
                                    <label for="cat" class="form-label">Edit category</label>
                                    <input type="text" class="form-control" id="cat" value="{{$categories->category_name}}" name="category_name" aria-describedby="emailHelp">

                                    @error('category_name')
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                    </div>
                                    <button type="submit" class="btn btn-primary">Update Category</button>
                                </form>
                            </div>
                    </div>
                </div>
            </div>
            </div>
    </div>


    @endsection









