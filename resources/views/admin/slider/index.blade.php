
@extends('admin.admin_master')


@section('admin')


    <div class="container">
        <h4 style="text-align: center">Home Slider</h4>

        <a href="{{route('add.slider')}}"><button class="btn btn-info">Add Slider</button></a>
        <div class="mt-2">
            <div class="row">
                <div class="col-md-12">
                        @if (session('success'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <strong>{{session('success')}}</strong>
                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                            </div>
                            @endif
                    <div class="card">
                        <div class="card-header">All sliders</div>
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">ID</th>
                                    <th scope="col">Title</th>
                                    <th scope="col">Description</th>
                                    <th scope="col">Image</th>
                                    <th scope="col">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $i=1;
                                    @endphp

                                    @foreach ($slider as $slide)


                                <tr>
                                    <th scope="row" width='5%'>{{$i++}}</th>
                                    <td width='10%'>{{$slide->title}}</td>
                                    <td  width='10%'>{{$slide->description}}</td>
                                    <td><img src="{{asset($slide->image)}}" width="200" height="100" alt=""></td>
                                    <td  width='20%'>
                                        <a class="btn btn-primary" href="{{route('edit.slider',['id'=>$slide->id])}}">edit</a>
                                        <a class="btn btn-danger" onclick="return confirm('Are you sure to delete')" href="{{route('delete.slider',['id'=>$slide->id])}}" >delete</a>
                                    </td>
                                        @endforeach
                                </tr>

                                </tbody>

                            </table>
                    </div>
                </div>
            </div>
            </div>
    </div>


    @endsection






