@extends('admin.admin_master')
@section('admin')


    <div class="container">
        <div class="mt-5">
            <div class="row">
                <div class="col-md-8">
                    @if (session('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>{{session('success')}}</strong>
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                    @endif
                    <div class="card">
                        <div class="card-header">Edit slider</div>
                        <div class="card-body">
                            <form action="{{route('update.slider',['id'=>$slider->id])}}" enctype="multipart/form-data" method="POST">
                                @csrf
                                <input type="hidden" name="old_image" value="{{$slider->image}}">
                                    <div class="mb-3">
                                    <label for="cat" class="form-label">Edit Title</label>
                                    <input type="text" class="form-control" id="cat" value="{{$slider->title}}"
                                            name="title" >

                                    @error('title')
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                    </div>
                                    <div class="mb-3">
                                        <label for="cat" class="form-label">description</label>
                                        <textarea class="form-control" id="cat"
                                                name="description" >{{$slider->description}}"</textarea>

                                        @error('description')
                                            <span class="text-danger">{{$message}}</span>
                                        @enderror
                                        </div>

                                    <div class="mb-3">
                                        <label for="cat" class="form-label">slide</label>
                                        <input type="file" class="form-control" id="cat" name="image"
                                                aria-describedby="emailHelp">

                                        @error('image')
                                            <span class="text-danger">{{$message}}</span>
                                        @enderror
                                        </div>
                                        <div class="form-group">
                                            <img src="{{asset($slider->image)}}" style="width:200px; height=;250px">
                                        </div>
                                    <button type="submit" class="btn btn-primary mt-2">Update slider</button>
                                </form>
                            </div>
                    </div>
                </div>
            </div>
            </div>
    </div>



    @endsection









