@extends('admin.admin_master')

@section('admin')

    <div class="container">
        <div class="mt-5">
            <div class="row">
                <div class="col-md-8">
                        {{-- @if (session('success'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <strong>{{session('success')}}</strong>
                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                            </div>
                            @endif --}}
                    <div class="card-group">
                        @foreach ($images as $image)
                        <div class="col-md-4 mt-5">
                            <div class="card">
                                <img src="{{asset($image->image)}}" alt="image">
                            </div>
                        </div>

                        @endforeach



                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header">add image</div>
                            <div class="card-body">


                                <form action="{{route('store.image')}}" enctype="multipart/form-data" method="POST">
                                    @csrf
                                    <div class="mb-3">
                                        <label for="cat" class="form-label">add image</label>
                                        <input type="file" class="form-control" id="cat" name="image[]"
                                            multiple='' >

                                        @error('image')
                                            <span class="text-danger">{{$message}}</span>
                                        @enderror
                                        </div>


                                    <button type="submit" class="btn btn-primary">Add image</button>
                                </form>
                            </div>
                    </div>
                </div>
            </div>
            </div>
    </div>



    @endsection


